import { defineNuxtConfig } from "nuxt/config";

export default defineNuxtConfig({
  ssr: false,
  css: ["~/assets/global.scss"],
  modules: ["@nuxtjs/color-mode", "@nuxtjs/i18n", "@pinia/nuxt"],
  i18n: {
    legacy: false,
    // lazy: true,
    // strategy: "no_prefix",
    strategy: "prefix_except_default",
    defaultLocale: "en-US",
    langDir: "locales",
    locales: [
      {
        code: "en-US",
        iso: "en-US",
        name: "English",
        file: "en.json",
      },
      {
        code: "zh-Cn",
        iso: "zh-Cn",
        name: "Chinese",
        file: "zh.json",
      },
    ],
  },
  colorMode: {
    preference: "dark", // default value of $colorMode.preference
    fallback: "dark", // fallback value if not system preference found
    hid: "nuxt-color-mode-script",
    globalName: "__NUXT_COLOR_MODE__",
    componentName: "ColorScheme",
    classPrefix: "",
    classSuffix: "-mode",
    storageKey: "nuxt-color-mode",
  },
  devServer: {
    host: "localhost",
    port: 3001,
  },
  // runtimeConfig: {
  //   apiSecret: "r%jZQpcMqRC1w%zBP1muzFqko2BD30sa",
  //   public: {
  //     baseURL: process.env.NUXT_PUBLIC_BASE_URL,
  //   },
  // },
  // router: {
  //   options: {
  //     strict: false
  //   }
  // },
});
