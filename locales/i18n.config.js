export default defineI18nConfig(() => ({
  legacy: false,
  lazy: true,
  // locale: 'en',
  strategy: 'no_prefix',
  defaultLocale: 'en-US',
  langDir: 'locales',
  locales: [
    {
      code: 'en-US',
      iso: 'en-US',
      name: 'English',
      file: 'en.json',
    },
    {
      code: 'zh-Cn',
      iso: 'zh-Cn',
      name: 'Chinese',
      file: 'zh.json',
    },       
  ],  
}))