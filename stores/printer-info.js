import { defineStore } from "pinia";

export const usePrinterStore = defineStore("printer-info", {
  state: () => ({
    printerInfo: {},
  }),
  getters: {},
  actions: {},
});
