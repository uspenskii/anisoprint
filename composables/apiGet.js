export const apiGet = (request, params) => { 
  return useFetch(
    request,
    {
      baseURL: 'http://localho.st:8088/api/v1/',
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },      
      onError: (error) => console.log("error", error),
      onRequest({ request, options }) {},
      onRequestError({ request, options, error }) {
        console.log(error);
      },
      onResponse({ request, response, options }) {
        return response._data;
      },
      onResponseError({ request, response, options }) {     
        console.log(response);
      },
    }
  );
};
