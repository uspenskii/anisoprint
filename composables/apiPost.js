export const apiPost = (request, body) => {  
  return useFetch(
    request,
    {
      baseURL: 'http://localho.st:8088/api/v1/',      
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: body,
      onError: (error) => console.log("error", error),
      onRequest({ request, options }) {},
      onRequestError({ request, options, error }) {
        console.log(error);
      },
      onResponse({ request, response, options }) {
        return response._data;
      },
      onResponseError({ request, response, options }) {     
        console.log(response);
      },
    },
    { watch: [body?.value] }
  );
};
